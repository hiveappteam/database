#!/bin/sh

set -e

# perform all actions as $POSTGRES_USER
export PGUSER="$POSTGRES_USER"

# create the 'hivebase' db
psql <<- EOSQL
    CREATE DATABASE hivebase;
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
EOSQL

/usr/bin/psql --username "postgres" --dbname "hivebase" < "/dump/schema.sql" || true
