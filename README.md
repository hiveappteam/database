# README
Dummy postgres container for hiveapp

## build
```
$ ./scripts/build.sh
```

## run
```
$ docker-compose up -d
```
