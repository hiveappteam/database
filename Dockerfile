FROM postgres:11

ENV POSTGRES_HOST_AUTH_METHOD trust

RUN mkdir -p /docker-entrypoint-initdb.d

WORKDIR dump
COPY ./install/schema.sql ./schema.sql
COPY ./install/initdb.sh /docker-entrypoint-initdb.d/initdb.sh
